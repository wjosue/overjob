import styles from '../styles/pages/Home.module.css';
import Header from '../src/components/Header';
import Cards from '../src/components/Cards';
import Filters from '../src/components/Filters';

export default function Home() {
  return (
    <div className={styles.structure}>
      <Header />
      <Filters />
      <Cards />
    </div>
  )
}
